package tdd.training.mra;

import java.util.ArrayList;

public class Rover {
	int x = 0;
	int y = 0;
	
	
	String orientation = "N";
	MarsRover planetMars = null;
	
	
	ArrayList<Cell> enconteredObstacles = new ArrayList<>();
	
	public Rover(int x, int y, String orientation, MarsRover planetMars) {
		this.x = x;
		this.y = y;
		this.orientation = orientation;
		
		this.planetMars = planetMars;
	}
	
	
	public void setX(int x) {
		this.x = x;
	}
	
	public int getX() {
		return x;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public int getY() {
		return y;
	}
	
	public void setOrientation(String orientation) {
		this.orientation = orientation;
	}
	
	public String getOrientation() {
		return orientation;
	}
	
	
	public String getCoordinate() {
		StringBuilder outputCoordinate = new StringBuilder();
		
		outputCoordinate.append("("+x+","+y+","+orientation+")");
		
		for(Cell cell : enconteredObstacles) {
			outputCoordinate.append("("+cell.x+","+cell.y+")");
		}
		
		return outputCoordinate.toString();
	}
	
	public void execute(String command) {
		if(command.equals("r")) {
			commandR();
		}else if(command.equals("l")) {
			commandL();
		}else if(command.equals("f")){
			commandF();
		}else if(command.equals("b")) {
			commandB();
		}		
	}


	private void commandB() {
		int tmpX = this.x;
		int tmpY = this.y;
			
		switch (orientation) {
			case "N":
				if(tmpY - 1 < 0) {
					tmpY = planetMars.planetY - 1;
				}else {
					tmpY -= 1;
				}
				break;
			case "S":
				if(tmpY + 1 >= planetMars.planetY) {
					tmpY = 0;
				}else {
					tmpY += 1;
				}
				break;
			case "E":
				if(tmpX - 1 < 0) {
					tmpX = planetMars.planetX - 1;
				}else {
					tmpX -= 1;
				}
				break;
			case "W":
				if(tmpX + 1 >= planetMars.planetX) {
					tmpX = 0;
				}else {
					tmpX += 1;
				}
				break;
			default:
		}
		
		obstacleHandler(tmpX, tmpY);
	}


	private void obstacleHandler(int tmpX, int tmpY) {
		if(!planetMars.planet[tmpX][tmpY].isObstacle()) {
			this.x = tmpX;
			this.y = tmpY;	
		}else {
			boolean flag = true;
			for(int i = 0; i < enconteredObstacles.size(); ++i) {
				if(enconteredObstacles.get(i).equals(planetMars.planet[tmpX][tmpY])) {
					flag = false;
				}
			}
			
			if(flag) {
				enconteredObstacles.add(planetMars.planet[tmpX][tmpY]);
			}
		}
	}


	private void commandF() {
		int tmpX = this.x;
		int tmpY = this.y;
		
		switch (orientation) {
			case "N":
				if(tmpY + 1 >= planetMars.planetY) {
					tmpY = 0;
				}else {
					tmpY += 1;	
				}
				
				break;
			case "S":
				if(tmpY - 1 < 0) {
					tmpY = planetMars.planetY - 1;
				}else {
					tmpY -= 1;
				}
				break;
			case "E":
				if(tmpX + 1 >= planetMars.planetX) {
					tmpX = 0;
				}else {
					tmpX += 1;
				}
				break;
			case "W":
				if(tmpX - 1 < 0) {
					tmpX = planetMars.planetX - 1;
				}else {
					tmpX -= 1;
				}
				break;
			default:
		}
					
		obstacleHandler(tmpX, tmpY);
	}


	private void commandL() {
		switch (orientation) {
				case "N":
					this.orientation = "W";
					break;
				case "S":
					this.orientation = "E";
					break;
				case "E":
					this.orientation = "N";
					break;
				case "W":
					this.orientation = "S";
					break;
				default:
			}
	}


	private void commandR() {
		switch (orientation) {
			case "N":
				this.orientation = "E";
				break;
			case "S":
				this.orientation = "W";
				break;
			case "E":
				this.orientation = "S";
				break;
			case "W":
				this.orientation = "N";
				break;
			default:
		}
	}
}
