package tdd.training.mra;

import java.util.List;

public class MarsRover {
	
	int planetX = 0;
	int planetY = 0;
	
	Cell[][] planet;
	
	Rover rover;

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		this.planetX = planetX;
		this.planetY = planetY;
		
		this.planet = new Cell[planetX][planetY];
		
		
		this.rover = new Rover(0, 0, "N", this);
		
		
		for(int i = 0; i < planetX; ++i) {
			for(int j = 0; j < planetY; ++j) {
				this.planet[i][j] = new Cell();
				this.planet[i][j].setX(i);
				this.planet[i][j].setY(j);
			}
		}
		
		
		for(int i = 0; i < planetObstacles.size(); ++i) {
			String[] obstacleCoordinate = planetObstacles.get(i).replace("(", "").replace(")", "").split(",");
			
			int obstacleX = Integer.parseInt(obstacleCoordinate[0].replace(" ", ""));
			int obstacleY = Integer.parseInt(obstacleCoordinate[1].replace(" ", ""));
			
			this.planet[obstacleX][obstacleY].setObstacle(true);
		}
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {		
		return planet[x][y].isObstacle();
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		if(commandString.length() == 1) {
			rover.execute(commandString);
		}else {
			for(int i = 0; i < commandString.length(); ++i) {
				String command = String.valueOf(commandString.charAt(i));
				rover.execute(command);
			}
		}
		
		return rover.getCoordinate();
	}

}
