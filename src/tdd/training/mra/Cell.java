package tdd.training.mra;

public class Cell {
	int x = 0;
	int y = 0;
	
	boolean obstacle = false;
	
	
	public void setX(int x) {
		this.x = x;
	}
	
	public int getX() {
		return x;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public int getY() {
		return y;
	}
	
	public void setObstacle(boolean state) {
		this.obstacle = state;
	}
	
	public boolean isObstacle() {
		return obstacle;
	}
	
}
