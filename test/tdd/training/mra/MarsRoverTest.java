package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testMarsRover() {
		try {
			List<String> planetObstacles = new ArrayList<>();
			
			planetObstacles.add("(5, 5)");
			planetObstacles.add("(7, 8)");
			
			MarsRover marseRover = new MarsRover(10, 10, planetObstacles);
			
		}catch (MarsRoverException e) {
			fail();
		}
	}
	
	@Test
	public void testMarsPlanetShouldContainsObstacleAt() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		
		planetObstacles.add("(5, 5)");
		planetObstacles.add("(7, 8)");
		
		MarsRover marseRover = new MarsRover(10, 10, planetObstacles);
		
		assertTrue(marseRover.planetContainsObstacleAt(5, 5));
	}
	
	@Test
	public void testMarsPlanetShouldNotContainsObstacleAt() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		
		planetObstacles.add("(5, 5)");
		planetObstacles.add("(7, 8)");
		
		MarsRover marseRover = new MarsRover(10, 10, planetObstacles);
		
		assertFalse(marseRover.planetContainsObstacleAt(5, 6));
	}
	
	@Test
	public void testRoverExecuteCommand() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		
		planetObstacles.add("(5, 5)");
		planetObstacles.add("(7, 8)");
		
		MarsRover marseRover = new MarsRover(10, 10, planetObstacles);
		
		assertTrue(marseRover.executeCommand("").equals("(0,0,N)"));
	}
	
	@Test
	public void testRoverShouldBeTurnToRight() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		
		planetObstacles.add("(5, 5)");
		planetObstacles.add("(7, 8)");
		
		MarsRover marseRover = new MarsRover(10, 10, planetObstacles);
		
		assertTrue(marseRover.executeCommand("r").equals("(0,0,E)"));
	}
	
	@Test
	public void testRoverShouldBeTurnToLeft() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		
		planetObstacles.add("(5, 5)");
		planetObstacles.add("(7, 8)");
		
		MarsRover marseRover = new MarsRover(10, 10, planetObstacles);
		
		assertTrue(marseRover.executeCommand("l").equals("(0,0,W)"));
	}
	
	@Test
	public void testRoverMoveForward() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		
		planetObstacles.add("(5, 5)");
		planetObstacles.add("(7, 8)");
		
		MarsRover marseRover = new MarsRover(10, 10, planetObstacles);
		
		marseRover.rover.setX(5);
		marseRover.rover.setY(5);
		marseRover.rover.setOrientation("E");
		
		assertTrue(marseRover.executeCommand("f").equals("(6,5,E)"));
	}
	
	@Test
	public void testRoverMoveBackword() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		
		planetObstacles.add("(5, 5)");
		planetObstacles.add("(7, 8)");
		
		MarsRover marseRover = new MarsRover(10, 10, planetObstacles);
		
		marseRover.rover.setX(5);
		marseRover.rover.setY(5);
		
		assertTrue(marseRover.executeCommand("b").equals("(5,4,N)"));
	}
	
	@Test
	public void testRoverMoveWithCombinationOfCommand() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		
		planetObstacles.add("(5, 5)");
		planetObstacles.add("(7, 8)");
		
		MarsRover marseRover = new MarsRover(10, 10, planetObstacles);
		
		assertTrue(marseRover.executeCommand("ffrff").equals("(2,2,E)"));
	}
	
	@Test
	public void testRoverShouldBeMoveToOppisiteEdge() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		
		planetObstacles.add("(2, 2)");
		planetObstacles.add("(1, 1)");
		
		MarsRover marseRover = new MarsRover(10, 10, planetObstacles);
		
		assertTrue(marseRover.executeCommand("b").equals("(0,9,N)"));
	}
	
	@Test
	public void testRoverShouldEconteredSingleObstacle() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		
		planetObstacles.add("(2, 2)");
		
		MarsRover marseRover = new MarsRover(10, 10, planetObstacles);
		
		assertTrue(marseRover.executeCommand("ffrfff").equals("(1,2,E)(2,2)"));
	}
	
	@Test
	public void testRoverShouldEconteredMultipleObstacle() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		
		planetObstacles.add("(2, 2)");
		planetObstacles.add("(2, 1)");

		MarsRover marseRover = new MarsRover(10, 10, planetObstacles);
		
		assertTrue(marseRover.executeCommand("ffrfffrflf").equals("(1,1,E)(2,2)(2,1)"));
	}
	
	@Test
	public void testRoverShouldEconteredObstacleOnEdge() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		
		planetObstacles.add("(0, 9)");

		MarsRover marseRover = new MarsRover(10, 10, planetObstacles);
		
		assertTrue(marseRover.executeCommand("b").equals("(0,0,N)(0,9)"));
	}
	
	@Test
	public void testRoverRoundThePlanet() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		
		planetObstacles.add("(2, 2)");
		planetObstacles.add("(0, 5)");
		planetObstacles.add("(5, 0)");


		MarsRover marseRover = new MarsRover(6, 6, planetObstacles);
		
		assertTrue(marseRover.executeCommand("ffrfffrbbblllfrfrbbl").equals("(0,0,N)(2,2)(0,5)(5,0)"));
	}
	
}
